package com.jira.plugin.snIntegrationPlugin.controller.impl;

import com.jira.plugin.snIntegrationPlugin.controller.api.Column;
import java.util.Comparator; 

public class SNColumns implements Column
{
	public String columnName;
	public String columnLabel;
	public String sysID;
	public String columnType;
	public String referenceTableName;

	public SNColumns(String columnName,String columnLabel,String sysID,String columnType,String referenceTableName)
	{
		this.columnName = columnName;
		this.columnLabel = columnLabel;
		this.sysID = sysID;
		this.columnType = columnType;
		this.referenceTableName = referenceTableName;
	}

	public String getColumnName()
	{
		return columnName;
	}
	public String getColumnLabel()
	{
		return columnLabel;
	}
	public String getSysID()
	{
		return sysID;
	}
	public String getColumnType()
	{
		return columnType;
	}
	public String getReferenceTableName()
	{
		return referenceTableName;
	}
	public static Comparator<Column> ColumnLabelComparator = new Comparator<Column>() {

	public int compare(Column column1, Column column2) {
	   String columnName1 = column1.getColumnLabel().toUpperCase();
	   String columnName2 = column2.getColumnLabel().toUpperCase();
	   //ascending order
	   return columnName1.compareTo(columnName2);
    }};
    
    public static Comparator<Column> ColumnNameComparator = new Comparator<Column>() {

	public int compare(Column column1, Column column2) {
	   String columnName1 = column1.getColumnName().toUpperCase();
	   String columnName2 = column2.getColumnName().toUpperCase();
	   //ascending order
	   return columnName1.compareTo(columnName2);
    }};
}