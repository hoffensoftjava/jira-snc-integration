package com.jira.plugin.snIntegrationPlugin.controller.XMLElement;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.List;
import javax.xml.bind.annotation.*;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLConnectionConfig
{
  @XmlElement public int id;
  @XmlElement public String appLinkid;
  @XmlElement public String appLinkURL;
  @XmlElement public String sncName;
  @XmlElement public String userName;
  @XmlElement public String password;
  @XmlElement public String authType;
  @XmlElement public int interval;
  @XmlElement public String duration;

}