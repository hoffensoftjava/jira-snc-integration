package com.jira.plugin.snIntegrationPlugin.controller.api;

public interface NameValueMapper
{
	public String getName();
	public String getValue();
}