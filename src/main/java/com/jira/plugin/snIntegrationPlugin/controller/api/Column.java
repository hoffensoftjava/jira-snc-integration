package com.jira.plugin.snIntegrationPlugin.controller.api;

public interface Column
{
	public String getColumnName();
	public String getColumnLabel();
	public String getColumnType();
	public String getReferenceTableName();
	public String getSysID();
}