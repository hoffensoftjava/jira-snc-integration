package com.jira.plugin.snIntegrationPlugin.controller.api;

public interface Table
{
	public String getName();
	public String getLabel();
	public String getSysID();
	public String getLink();
	public String getLinkValue();
}