package com.jira.plugin.snIntegrationPlugin.controller.impl;

import com.jira.plugin.snIntegrationPlugin.controller.api.Table;
import java.util.Comparator; 

public class SNTables implements Table
{
	public String name;
	public String label;
	public String sysID;
	public String link;
	public String linkValue;

	public SNTables(String name,String label,String sysID,String link,String linkValue)
	{
		this.name = name;
		this.label = label;
		this.sysID = sysID;
		this.link = link;
		this.linkValue = linkValue;
	}

	public String getName()
	{
		return name;
	}
	public String getLabel()
	{
		return label;
	}
	public String getSysID()
	{
		return sysID;
	}
	public String getLink()
	{
		return link;
	}
	public String getLinkValue()
	{
		return linkValue;
	}
	public static Comparator<Table> TableLabelComparator = new Comparator<Table>() {

	public int compare(Table table1, Table table2) {
	   String TableName1 = table1.getLabel().toUpperCase();
	   String TableName2 = table2.getLabel().toUpperCase();
	   //ascending order
	   return TableName1.compareTo(TableName2);
    }};
    
    public static Comparator<Table> TableNameComparator = new Comparator<Table>() {

	public int compare(Table table1, Table table2) {
	   String TableName1 = table1.getName().toUpperCase();
	   String TableName2 = table2.getName().toUpperCase();
	   //ascending order
	   return TableName1.compareTo(TableName2);
    }};
}