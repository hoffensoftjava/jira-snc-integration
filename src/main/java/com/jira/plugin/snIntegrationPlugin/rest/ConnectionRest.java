package com.jira.plugin.snIntegrationPlugin.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

 import javax.servlet.http.HttpServletRequest;
 import javax.ws.rs.Consumes;
 import javax.ws.rs.GET;
 import javax.ws.rs.POST;
 import javax.ws.rs.DELETE;
 import javax.ws.rs.PUT;
 import javax.ws.rs.Path;
 import javax.ws.rs.Produces;
 import javax.ws.rs.core.Context;
 import javax.ws.rs.core.Response.Status;

 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;

 import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
 import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
 import com.atlassian.jira.component.ComponentAccessor;
 import com.atlassian.jira.security.JiraAuthenticationContext;
 import com.atlassian.jira.web.action.JiraWebActionSupport;
 import com.atlassian.jira.user.ApplicationUser;

 import java.util.ArrayList; 
 import java.util.List;
 import java.util.*;
 import java.util.Set;
 import javax.inject.Inject;
 import javax.inject.Named;


 import com.atlassian.sal.api.net.RequestFactory; 
 import com.atlassian.sal.api.user.*;
 import com.atlassian.sal.api.auth.LoginUriProvider;
 import com.atlassian.sal.api.pluginsettings.PluginSettings;
 import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
 import com.atlassian.sal.api.user.UserManager;
 import com.atlassian.sal.api.transaction.TransactionCallback;
 import com.atlassian.sal.api.transaction.TransactionTemplate;

import com.jira.plugin.snIntegrationPlugin.service.integration.ServiceNowClient;
import com.jira.plugin.snIntegrationPlugin.entity.ConnectionConfig;
import com.jira.plugin.snIntegrationPlugin.service.aoservice.AOMappingService;
import com.jira.plugin.snIntegrationPlugin.controller.XMLElement.XMLConnectionConfig;
import com.jira.plugin.snIntegrationPlugin.exceptions.HSResponseException;
import com.jira.plugin.snIntegrationPlugin.exceptions.HSJSONNotFormattedException;


@Path("/connectionconfig")
@Named("ConnectionRest")
public class ConnectionRest 
{
	  @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	  @ComponentImport
    private final TransactionTemplate transactionTemplate;
    private AOMappingService aomappingservice;
    private ServiceNowClient serviceNowClient;
    String jsonErrorMessage;
    

    private static final Logger log;
    static 
    {
        log = LoggerFactory.getLogger(ConnectionRest.class);
    }

    @Inject
  	public ConnectionRest(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,TransactionTemplate transactionTemplate, AOMappingService aomappingservice,ServiceNowClient serviceNowClient)
        
  	{
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.transactionTemplate = transactionTemplate;
        this.aomappingservice = aomappingservice;
        this.serviceNowClient = serviceNowClient;
        
  	}


  @GET
  @Path("/connectionconfigs/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getConnectionConfig(@PathParam("id") final int xmlconfigid)
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              ConnectionConfig connectionVariables = aomappingservice.getConnectionConfig(xmlconfigid);
              return loadConnectionIntoXML(connectionVariables);
          }

        catch(Exception e)
        {
            throw new RuntimeException(e);
        }

      }
    })).build();
  }


  //Test Connection rest
  @GET
  @Path("/testconnection/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response testConnectionId(@PathParam("id") final int xmlconfigid)
  {
     

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
          StringBuilder output = new StringBuilder();
          jsonErrorMessage="";
          output.append(jsonErrorMessage);
        try
          {
              serviceNowClient.checkValidConnection(xmlconfigid);
          }

          catch(HSResponseException e)
            {
                log.error("HSResponseException Log is {}",((HSResponseException)e).getErrorString());
                jsonErrorMessage = ((HSResponseException)e).getErrorString(); 
                output.append(jsonErrorMessage);
            }
            catch(HSJSONNotFormattedException e)
            {
                log.error("HSJSONNotFormattedException Log is {}",e.getMessage());
                jsonErrorMessage = ((HSJSONNotFormattedException)e).getJSONString();  
                output.append(jsonErrorMessage); 
            }

      return output.toString();
      }
    })).build();
  }


  @GET
  @Path("/connectionconfigs")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getConnectionConfig()
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        
        try
          {
              Iterable<ConnectionConfig> connectionVariables = aomappingservice.getConnectionConfig();
              return loadConnectionListIntoXML(connectionVariables);
              
          }

        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
      }
    })).build();
  }


    private List<XMLConnectionConfig> loadConnectionListIntoXML(Iterable<ConnectionConfig> connectionVariables)
      {

            List<XMLConnectionConfig> xmlconfigList = new ArrayList<XMLConnectionConfig>(); 
            for(ConnectionConfig conn : connectionVariables) {
              xmlconfigList.add(loadConnectionIntoXML(conn));
            }
            return xmlconfigList;
      }



    private XMLConnectionConfig loadConnectionIntoXML(ConnectionConfig connectionconfig)
    {
       
       XMLConnectionConfig xmlconfig = new XMLConnectionConfig();
       xmlconfig.id = connectionconfig.getID();
       xmlconfig.appLinkid = connectionconfig.getAppLinkid();
       xmlconfig.appLinkURL = connectionconfig.getAppLinkURL();
       xmlconfig.sncName = connectionconfig.getSncName();
       xmlconfig.userName = connectionconfig.getUserName();
       xmlconfig.password = connectionconfig.getPassword();
       xmlconfig.authType = connectionconfig.getAuthType();
       xmlconfig.interval = connectionconfig.getInterval();
       xmlconfig.duration = connectionconfig.getDuration().toString();
       return xmlconfig;
    }


    @DELETE
    @Path("/connectionconfigs/{id}")
    @Produces("text/plain")
    public Response deleteConnectionconfig( @PathParam("id") final int connID)
    {

      // String username = userManager.getRemoteUsername();
      // if (username == null || !userManager.isSystemAdmin(username))
      // {
      //   return Response.status(Status.UNAUTHORIZED).build();
      // }
      transactionTemplate.execute(new TransactionCallback()
      {
        @Override
        public Object doInTransaction()
        {
        try
        {
          ConnectionConfig conn = aomappingservice.getConnectionConfig(connID);
          aomappingservice.deleteConnectionconfig(conn);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return null;
      }
    });
    return Response.noContent().build();
  }





}