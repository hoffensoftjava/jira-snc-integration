package com.jira.plugin.snIntegrationPlugin.webwork;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.collect.Maps;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.user.ApplicationUser;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.Enumeration;
//import net.java.ao.Query;

import java.util.ArrayList; 
import java.util.List;
import java.util.*;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 

import com.atlassian.sal.api.user.UserManager;


import com.jira.plugin.snIntegrationPlugin.controller.api.Table;
import com.jira.plugin.snIntegrationPlugin.controller.api.Column;
import com.jira.plugin.snIntegrationPlugin.controller.api.NameValueMapper;
import com.jira.plugin.snIntegrationPlugin.controller.impl.SNTables;
import com.jira.plugin.snIntegrationPlugin.controller.impl.SNColumns;
import com.jira.plugin.snIntegrationPlugin.controller.impl.NameValueFactory;


import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.NavigableField;
import java.util.Set;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import com.jira.plugin.snIntegrationPlugin.service.aoservice.AOMappingService;
import com.jira.plugin.snIntegrationPlugin.entity.ConnectionConfig;



@Named("getMappingConfig")
public class getMappingConfig extends JiraWebActionSupport
{
    private static final Logger log;

    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final RequestFactory requestFactory; 
    @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final ProjectManager projectManager;



    static
    {
    	
    	log=LoggerFactory.getLogger(getMappingConfig.class);
    }


    private AOMappingService aomappingservice;
    private ConnectionConfig connectionconfig;

   



    @Inject
    public getMappingConfig(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,AOMappingService aomappingservice,ProjectManager projectManager)
    {
        super();
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.aomappingservice = aomappingservice ;
        this.projectManager = projectManager;         
    }


    @Override
    protected void doValidation()
    {
        log.info("Entering doValidation");
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
        String n = (String) e.nextElement();
        String[] vals = request.getParameterValues(n);
        log.debug("name " + n + ": " + vals[0]);
        }
    }



    @Override
    public String execute() throws Exception {

        return super.execute(); //returns SUCCESS
    }

     @Override
    public String doDefault() throws Exception 
    {
        return INPUT;
    }

    


}