package com.jira.plugin.snIntegrationPlugin.exceptions;

public class HSInputValidationException extends RuntimeException 
{
        private final String field;

        public HSInputValidationException(final String message, final String field) {
            super(message);
            this.field = field;
        }

        public String getField() {
            return field;
        }
    }