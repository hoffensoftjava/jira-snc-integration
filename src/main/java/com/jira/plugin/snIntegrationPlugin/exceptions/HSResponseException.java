package com.jira.plugin.snIntegrationPlugin.exceptions;


import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.jira.util.json.*;

public class HSResponseException extends ResponseException {
        private final String errorString;

        public HSResponseException(final String message, final String errorString) {
            super(message);
            this.errorString = errorString;
        }

        public String getErrorString() {
            return errorString;
        }
    }