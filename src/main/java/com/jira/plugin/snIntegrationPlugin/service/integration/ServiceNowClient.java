package com.jira.plugin.snIntegrationPlugin.service.integration;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.inject.Inject;
import net.java.ao.EntityManager;
import net.java.ao.Query;

import org.apache.commons.lang.StringUtils;
import java.text.SimpleDateFormat;
import com.atlassian.jira.component.ComponentAccessor;
//import com.atlassian.jira.ComponentManager;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;

import com.jira.plugin.snIntegrationPlugin.exceptions.HSResponseException;
import com.jira.plugin.snIntegrationPlugin.exceptions.HSJSONNotFormattedException;

import com.jira.plugin.snIntegrationPlugin.service.integration.SNIntegratonResponseHandler;
import com.jira.plugin.snIntegrationPlugin.utils.helper.*;

import com.jira.plugin.snIntegrationPlugin.controller.impl.NameValueFactory;
import com.jira.plugin.snIntegrationPlugin.controller.api.NameValueMapper;
import com.jira.plugin.snIntegrationPlugin.service.aoservice.AOMappingService;
import com.jira.plugin.snIntegrationPlugin.entity.ConnectionConfig;
import com.jira.plugin.snIntegrationPlugin.exceptions.*;

@ExportAsService
@Component
public class ServiceNowClient
{
	private String changeRequestEndPoint = "/change_request";
	private String snBaseUrl = "";//ConfigHelper.getConnectionConfig().getEndpointURL();//https://dev10629.service-now.com/api/now/table/";
	private String schemasEndPoint = "";//https://dev10629.service-now.com/api/now/table/sys_db_object";
	private String tableColumnEndPoint = "";//https://dev10629.service-now.com/api/now/table/sys_dictionary";
	private String username = "";
	private String password = "";//Mahis!234
	private String contentType = "";
    private int testing_id; // Id recieved from servelt on test action

	public enum SERVICENOW_INTEGRATION 
	{
        CREATE_ISSUE,
        UPDATE_ISSUE,
        FETCH_ALL_TABLES,
        FETCH_PARENT_TABLES,
        RETRIEVE_UPDATES,
        FETCH_SPECIAL_VALUES,
        FETCH_ALL_COLUMNS
    }

    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");

    @ComponentImport
	private final RequestFactory requestFactory; 
	@ComponentImport
	private final PluginSettingsFactory pluginSettingsFactory;
	@ComponentImport
    protected final ActiveObjects activeObjects;
	private static final Logger log = LoggerFactory.getLogger(ServiceNowClient.class);


	@Inject
    public ServiceNowClient(final RequestFactory requestFactory,final PluginSettingsFactory pluginSettingsFactory,
    	final ActiveObjects activeObjects)
    {
         this.requestFactory = requestFactory;
         this.pluginSettingsFactory = pluginSettingsFactory;
         this.activeObjects = activeObjects;
         log.debug("this.activeObjects {}",this.activeObjects);
    }


// Check Valid Connection
    public void checkValidConnection(int testing_id) throws HSResponseException,HSJSONNotFormattedException
    {
        this.testing_id=testing_id;
        String jsonRequest = "";
        try
        {
           jsonRequest = fetchAllTables(); 
        }
        catch(HSResponseException e)
        {
            log.error(e.getErrorString());
            throw new HSResponseException(e.getErrorString(),e.getErrorString());
        }
        try
        {
           
            JSONArray jsonResult = new JSONObject(jsonRequest).getJSONArray("result");
            for (int i = 0; i < jsonResult.length(); i++)
            {
                JSONObject tableJSON = jsonResult.getJSONObject(i);
                String name = tableJSON.getString("name");
                String label = tableJSON.getString("label");
                String sysid = tableJSON.getString("sys_id");
                String link  = "";
                String value = "";
                if(tableJSON.has("sys_package") && !tableJSON.isNull("sys_package") && !tableJSON.getString("sys_package").isEmpty())
                {
                    JSONObject jsonSysPackage = tableJSON.getJSONObject("sys_package");
                    link = jsonSysPackage.getString("link");
                    value = jsonSysPackage.getString("value");
                }
            }
        }
        catch(JSONException e)
        {
            log.error("Looks like the Servicenow endpoint response is not in expected format in Connection Testing(){}",jsonRequest);
            throw new HSJSONNotFormattedException("Servicenow endpoint response is not in expected format in ConnectionTesting() function"+jsonRequest,jsonRequest);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    } 

    //Fetch All Tables
    public String fetchAllTables() throws HSResponseException
    {   
        loadConfigSettings();
        String requestURL="";
        try
        {
            requestURL = String.format("sysparm_query=%s",URLEncoder.encode("is_extendable!=false","UTF-8"));       
        }
         catch (UnsupportedEncodingException e) 
        { 
            log.error("fetchAllTables - Error generating UTF8 encoding.", e); 
        }
         
        requestURL = schemasEndPoint +"?"+ requestURL;
        log.info("Endpoint for fetchAllTables : {}", requestURL);
        String response = executeAPIRequest(Request.MethodType.GET,SERVICENOW_INTEGRATION.FETCH_ALL_TABLES,requestURL);
        return response;
    }

    //Load Configuration Settings
    private void loadConfigSettings()
    {

     ConnectionConfig connectionConfig = ConfigHelper.getConnectionConfig(testing_id);
     contentType = ConfigHelper.CONTENT_TYPE;
     snBaseUrl = connectionConfig.getAppLinkURL();
     if(snBaseUrl.substring(snBaseUrl.length() - 1) != "/")
        snBaseUrl = snBaseUrl +"/";
     snBaseUrl = snBaseUrl.toLowerCase().contains(ConfigHelper.TABLE_API)?snBaseUrl : snBaseUrl + ConfigHelper.TABLE_API;
     schemasEndPoint = snBaseUrl.toLowerCase().contains(snBaseUrl+ConfigHelper.TABLE_NAME)? snBaseUrl : snBaseUrl + ConfigHelper.TABLE_NAME;
     tableColumnEndPoint = snBaseUrl.toLowerCase().contains(snBaseUrl + ConfigHelper.COLUMN_TABLE_NAME)? snBaseUrl : snBaseUrl + ConfigHelper.COLUMN_TABLE_NAME;
     username = connectionConfig.getUserName();
     password = connectionConfig.getPassword();

    }

    //Execute API Request
    private String executeAPIRequest(Request.MethodType methodType,SERVICENOW_INTEGRATION integration_Type,String requestUrl) throws HSResponseException
    {
        Request request = requestFactory.createRequest(methodType, requestUrl);
        return executeAPIRequest(request,integration_Type,requestUrl);
    }


    private String executeAPIRequest(Request request,SERVICENOW_INTEGRATION integration_Type,String requestUrl) throws HSResponseException
    {
        String responseJSON = "";
        SNIntegratonResponseHandler handler = new SNIntegratonResponseHandler();
        String requestDetails = String.format("EndpointURL : %s; Integration Type : %s;",requestUrl,integration_Type);  
        try
        {
        request.setHeader("Content-Type", contentType);
        request.setHeader("Authorization", getBasicAuthString());
        request.setHeader("Accept-Charset", "UTF-8");
        request.execute(handler);
        if(handler.isSuccessful())
        {
            responseJSON = handler.getResponse();
        }
        else
        {
         String exceptionString = String.format("Request Failed; Reason: %d-%s;Details: %s ",
                        handler.getStatusCode(),handler.getStatusText(),requestDetails);
         log.error(exceptionString +"{}",responseJSON);
         throw new HSResponseException("Exception in executing the API",exceptionString);
         
        }
        log.debug("response: {} : details : {} ",responseJSON,requestDetails);
        return responseJSON;
        }
        catch(ResponseException e)
        {
            String exceptionString = String.format("Request Failed; Reason: %d-%s;Details: %s ",
                        handler.getStatusCode(),handler.getStatusText(),requestDetails);
         log.error(exceptionString +"{}",responseJSON);
         throw new HSResponseException("Exception in executing the API",exceptionString);
        }
    }
    
    // Get encoded String
    public String getBasicAuthString()
    {
        String authString = username + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        log.debug("Basic Authstring Basic {}",authStringEnc);

        return "Basic " + authStringEnc;
    }

    

}