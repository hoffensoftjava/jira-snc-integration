package com.jira.plugin.snIntegrationPlugin.service.integration;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class SNIntegratonResponseHandler implements ResponseHandler<Response> {
    private boolean successful;
    private String responseBody = "";
    private int statusCode;
    private String statusText = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(SNIntegratonResponseHandler.class);

    @Override
    public void handle(final Response response) throws ResponseException {
        successful = response.getStatusCode() >= 200 && response.getStatusCode() < 300;
        responseBody = response.getResponseBodyAsString();
        statusCode = response.getStatusCode();
        statusText = response.getStatusText();
    }
    public int getStatusCode()
    {
        return statusCode;
    }
    public String getStatusText()
    {
        return statusText;
    }
    public boolean isSuccessful() 
    {
        return successful;
    }

    public String getResponse() 
    {
        return responseBody;
    }
}