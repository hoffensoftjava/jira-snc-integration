package com.jira.plugin.snIntegrationPlugin.servlet;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response; 
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ResponseException; 
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.RequestFactory; 
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.ArrayList; 
import java.util.List;
import com.jira.plugin.snIntegrationPlugin.service.aoservice.AOMappingService;
// import com.jira.plugin.snIntegrationPlugin.service.integration.CronService;
import com.jira.plugin.snIntegrationPlugin.service.integration.ServiceNowClient;
import com.jira.plugin.snIntegrationPlugin.entity.ConnectionConfig;
import com.jira.plugin.snIntegrationPlugin.controller.XMLElement.XMLConnectionConfig;
import com.jira.plugin.snIntegrationPlugin.rest.ConnectionRest;
import com.jira.plugin.snIntegrationPlugin.service.integration.SNIntegratonResponseHandler;
import com.jira.plugin.snIntegrationPlugin.exceptions.HSResponseException;
import com.jira.plugin.snIntegrationPlugin.exceptions.HSJSONNotFormattedException;

import org.apache.commons.codec.binary.Base64;

import com.atlassian.sal.api.user.UserManager;

@Scanned
public class AdminServlet extends javax.servlet.http.HttpServlet 
{
	
    private static final Logger log;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    private AOMappingService aoMappingService;
    private Request request;
 // private CronService cronService;
    private ServiceNowClient serviceNowClient;


    private static final String ADMIN_TEMPLATE = "/templates/connections/AddSNCConnection.vm";
    private static final String SAVE = "Save Settings";
    private static final String TEST = "Test";
    private String test_id;

    static 
    {
        log = LoggerFactory.getLogger(AdminServlet.class);
    }

    @Inject
    public AdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, 
        TemplateRenderer templateRenderer, PluginSettingsFactory pluginSettingsFactory,
        AOMappingService aoMappingService,ServiceNowClient serviceNowClient)
    {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.aoMappingService = aoMappingService;
        // this.cronService = cronService;
        this.serviceNowClient = serviceNowClient;
    }



    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException
    {
        UserKey userkey = userManager.getRemoteUserKey(req);
        if (userkey == null || !userManager.isSystemAdmin(userkey)) 
        {
            redirectToLogin(req, response);
            return;
        }
        Map<String, Object> context = Maps.newHashMap();
        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render(ADMIN_TEMPLATE,context,response.getWriter());
    }

     

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException
    {
        String action = req.getParameter("action");
        log.debug("action: {}",req.getParameter("action"));
        String jsonErrorMessage = "";
        test_id = (String) req.getParameter("test_id");
        int tst_id;
        Map<String, Object> context = Maps.newHashMap();
        if(SAVE.equals(action))
        {
            ConnectionConfig connectionConfig = aoMappingService.submitConnectionConfig(req.getParameter("SNC_APPLINKURL"),req.getParameter("SNC_URL"),req.getParameter("SNC_NAME"),req.getParameter("SNC_USERNAME"),
<<<<<<< HEAD
                req.getParameter("SNC_PASSWORD"),ConnectionConfig.AuthType.valueOf(req.getParameter("SNC_AUTH_TYPE")),Integer.parseInt(req.getParameter("interval_time")),
                ConnectionConfig.Duration.valueOf(req.getParameter("interval_duration")));
=======
                req.getParameter("SNC_PASSWORD"),req.getParameter("SNC_AUTH_TYPE"),Integer.parseInt(req.getParameter("interval_time")),
<<<<<<< HEAD
                req.getParameter("interval_duration"));
>>>>>>> e9b08ed60122dfbc34b366a9828b0c89fb6d4464
=======
                ConnectionConfig.Duration.valueOf(req.getParameter("interval_duration")));
>>>>>>> 87ce2b5116eb881e9a3a388044d0a991f4879bea
        }
            response.setContentType("text/html;charset=utf-8");
            templateRenderer.render(ADMIN_TEMPLATE, context, response.getWriter());
        
    }



/* private long getIntervalMillisec(ConnectionConfig connectionConfig) 
 {

            long intervalSeconds = 5*60*1000;
            if(connectionConfig!=null)
            {
                if(connectionConfig.getDuration().toString() == "MINUTES")
                {
                    intervalSeconds = (long)(connectionConfig.getInterval() * 60 * 1000);
                }
                else if(connectionConfig.getDuration().toString() == "HOURS")
                {
                    intervalSeconds =  (long)(connectionConfig.getInterval() * 60 * 60 * 1000);
                }
                else if(connectionConfig.getDuration().toString() == "DAYS")
                {
                    intervalSeconds =  (long)(connectionConfig.getInterval() * 24 * 60 * 60 * 1000);
                }
            }
            return intervalSeconds;
        }
*/

     private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException 
     {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
     }

     private URI getUri(HttpServletRequest request) 
     {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) 
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }


}