package ut.com.jira.plugin.snIntegrationPlugin;

import org.junit.Test;
import com.jira.plugin.snIntegrationPlugin.controller.api.MyPluginComponent;
import com.jira.plugin.snIntegrationPlugin.controller.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}